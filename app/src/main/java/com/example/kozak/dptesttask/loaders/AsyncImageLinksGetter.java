package com.example.kozak.dptesttask.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.kozak.dptesttask.model.Image;

import java.util.List;

public class AsyncImageLinksGetter extends AsyncTaskLoader<List<Image>> {
    private static final String TAG = "AsyncImageLinksGetter";
    private String searchQuery;
    private String startPage;

    public AsyncImageLinksGetter(Context context, String searchQuery) {
        this(context, searchQuery, null);
    }

    public AsyncImageLinksGetter(Context context, String searchQuery, String startPage) {
        super(context);
        this.searchQuery = searchQuery;
        this.startPage = startPage;
    }

    @Override
    public List<Image> loadInBackground() {
        AsyncJsonGetter asyncJsonGetter = new AsyncJsonGetter();

        if (startPage == null) {
            asyncJsonGetter.execute(searchQuery);
        } else {
            asyncJsonGetter.execute(searchQuery, startPage);
        }
        return null;
    }
}
