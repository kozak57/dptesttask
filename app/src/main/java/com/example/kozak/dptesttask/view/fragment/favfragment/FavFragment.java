package com.example.kozak.dptesttask.view.fragment.favfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kozak.dptesttask.DataAdapter;
import com.example.kozak.dptesttask.R;
import com.example.kozak.dptesttask.ScrollListener;
import com.example.kozak.dptesttask.model.Image;
import com.example.kozak.dptesttask.view.fragment.FullScreenImageFragment;

import java.util.ArrayList;
import java.util.List;

public class FavFragment extends Fragment implements FavFragmentView {

    private static final String TAG = "FavFragment";
    private DataAdapter dataAdapter;
    private RecyclerView recyclerView;
    private FavFragmentPresenter presenter;
    private static List<Image> images;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fav_fragment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        presenter = new FavFragmentPresenterImpl(this, getContext());

        recyclerView.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new ScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int totalItemsCount) {
                presenter.showImages(totalItemsCount);
            }
        });
        images = new ArrayList<>();

        dataAdapter = new DataAdapter(images);
        recyclerView.setAdapter(dataAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.showImages(0);
        dataAdapter.notifyDataSetChanged();
    }

    @Override
    public void showResult(List<Image> imageList) {
        images.addAll(imageList);
        dataAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateResult(Image image, boolean isAdded) {
        if (isAdded) {
            images.add(image);
        } else {
            images.remove(image);
        }
        dataAdapter.notifyDataSetChanged();
    }

    @Override
    public void showFullScreenImage(String imageUrl) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Fragment fullScreenImageFragment = new FullScreenImageFragment();
        Bundle args = new Bundle();
        args.putString("ImageUrl", imageUrl);
        fullScreenImageFragment.setArguments(args);

        fragmentTransaction.add(fullScreenImageFragment, "fullscreen");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
