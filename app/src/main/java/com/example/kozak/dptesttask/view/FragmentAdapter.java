package com.example.kozak.dptesttask.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.kozak.dptesttask.view.fragment.favfragment.FavFragment;
import com.example.kozak.dptesttask.view.fragment.webfragment.WebFragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    private static final int FRAGMENT_SIZE = 2;

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new WebFragment();
                break;
            case 1:
                fragment = new FavFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return FRAGMENT_SIZE;
    }


}
