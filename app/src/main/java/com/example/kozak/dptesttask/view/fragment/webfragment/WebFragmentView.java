package com.example.kozak.dptesttask.view.fragment.webfragment;

import com.example.kozak.dptesttask.model.Image;
import com.example.kozak.dptesttask.view.fragment.FragmentView;

import java.util.List;

public interface WebFragmentView extends FragmentView {
    void updateResult(List<Image> images);

}
