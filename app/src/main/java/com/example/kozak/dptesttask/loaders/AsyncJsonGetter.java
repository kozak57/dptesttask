package com.example.kozak.dptesttask.loaders;

import android.os.AsyncTask;
import android.util.Log;

import com.example.kozak.dptesttask.model.Image;
import com.example.kozak.dptesttask.view.fragment.webfragment.WebFragmentPresenterImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AsyncJsonGetter extends AsyncTask<String, Void, List<Image>> {
    private static final String TAG = "AsyncJsonGetter";

    private static final String BASE_URL = "https://www.googleapis.com/customsearch/";
    private static final String API_KEY = "AIzaSyBLfVZhvS9Y7lepJmuX5zStL6I8FZgXEAk";
    private static final String SEARCH_ENGINE = "013063135215919321493:agikrrlgxt4";
    private static final String FIELDS = "items(title,pagemap/cse_image/src)";

    @Override
    protected List<Image> doInBackground(String... params) {
        JSONArray jsonArray = new JSONArray();
        List<Image> images = new ArrayList<>();
        URL url;
        String urlString;
        if (params.length > 1) {
            urlString = getUrl(params[0], params[1]);
        } else {
            urlString = getUrl(params[0]);
        }

        HttpURLConnection urlConnection = null;
        try {
            url = new URL(urlString);

            Log.d(TAG, url.toString());
            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = urlConnection.getInputStream();

            BufferedReader bR = new BufferedReader(new InputStreamReader(in));
            String line;

            StringBuilder responseStrBuilder = new StringBuilder();
            while ((line = bR.readLine()) != null) {

                responseStrBuilder.append(line);
            }
            in.close();

            jsonArray = new JSONObject(responseStrBuilder.toString()).getJSONArray("items");

        } catch (Exception e) {
            e.printStackTrace();
        } finally

        {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            Image image = new Image();
            try {
                JSONObject rootObject = jsonArray.getJSONObject(i);

                image.setTitle(rootObject.getString("title"));
                if (rootObject.has("pagemap")) {
                    image.setImageUrl(rootObject.getJSONObject("pagemap").getJSONArray("cse_image").getJSONObject(0).getString("src"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            images.add(image);
        }
        return images;
    }

    @Override
    protected void onPostExecute(List<Image> images) {

        WebFragmentPresenterImpl.updateView(images);
    }

    private String getUrl(String query, String startItem) {
        StringBuilder url = new StringBuilder();
        //https://www.googleapis.com/customsearch/v1?key=AIzaSyBLfVZhvS9Y7lepJmuX5zStL6I8FZgXEAk&cx=013063135215919321493:agikrrlgxt4&filetype=image&fields=items(title,pagemap/cse_image/src,pagemap/cse_thumbnail/src)&q=cat
        url.append(BASE_URL)
                .append("v1?")
                .append("key=")
                .append(API_KEY)
                .append("&cx=")
                .append(SEARCH_ENGINE)
                .append("&fields=")
                .append(FIELDS)
                .append("&start=")
                .append(startItem)
                .append("&q=")
                .append(query);

        return url.toString();
    }

    private String getUrl(String query) {
        return getUrl(query, "1");
    }
}
