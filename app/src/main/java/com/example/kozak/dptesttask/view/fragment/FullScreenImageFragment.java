package com.example.kozak.dptesttask.view.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.kozak.dptesttask.R;
import com.example.kozak.dptesttask.util.ImageGetter;

public class FullScreenImageFragment extends DialogFragment {

    private static final String TAG = "FullScreenFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "Full screenFragment");
        View view = inflater.inflate(R.layout.full_screen_image_fragment, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.full_screen_image);
        String imageUrl = getArguments().getString("ImageUrl");
        ImageGetter.getImage(imageUrl, imageView);
        return view;
    }
}