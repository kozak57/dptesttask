package com.example.kozak.dptesttask.model;

public class ContractClass {

    public static final class Image {
        public static final String TABLE_NAME = "images";

        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_URL = "url";


        public static final String[] DEFAULT_PROJECTION = new String[]{
                Image.COLUMN_NAME_TITLE,
                Image.COLUMN_NAME_URL,

        };
    }
}
