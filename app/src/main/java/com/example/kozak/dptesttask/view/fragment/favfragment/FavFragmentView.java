package com.example.kozak.dptesttask.view.fragment.favfragment;


import com.example.kozak.dptesttask.model.Image;
import com.example.kozak.dptesttask.view.fragment.FragmentView;

public interface FavFragmentView extends FragmentView {
    void updateResult(Image image, boolean isAdded);
}
