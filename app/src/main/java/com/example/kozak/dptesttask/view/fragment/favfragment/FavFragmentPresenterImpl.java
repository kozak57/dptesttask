package com.example.kozak.dptesttask.view.fragment.favfragment;

import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.util.Log;

import com.example.kozak.dptesttask.dao.ImageDao;
import com.example.kozak.dptesttask.loaders.FavImageLoader;
import com.example.kozak.dptesttask.model.ContractClass;
import com.example.kozak.dptesttask.model.Image;

import java.util.ArrayList;
import java.util.List;

public class FavFragmentPresenterImpl implements FavFragmentPresenter, Loader.OnLoadCompleteListener<List<Image>> {
    private static final String TAG = "FavFragment";
    private static FavFragmentView fragmentView;
    private ImageDao imageDao;

    private static Context context;

    public FavFragmentPresenterImpl(FavFragmentView fragment, Context context1) {
        fragmentView = fragment;
        context = context1;
        imageDao = ImageDao.getInstance(context);
    }

    public static void addToFav(Image image) {
        image.setInFavorites(true);
        ImageDao.getInstance(context).addImage(image);
        fragmentView.updateResult(image, true);
    }


    public static void removeFromFav(Image image) {
        ImageDao.getInstance(context).removeImage(image);
        fragmentView.updateResult(image, false);

    }

    @Override
    public void showImages(int totalCount) {
        Log.d(TAG, "Total totalCount" + totalCount);

        int count = totalCount + 10;

        FavImageLoader loader = new FavImageLoader(context);
        Cursor cursor = loader.loadInBackground();

        List<Image> images = new ArrayList<>();
        cursor.move(totalCount);
        while (cursor.moveToNext() && totalCount < count) {
            Image image = new Image();
            image.setTitle(cursor.getString(cursor.getColumnIndex(ContractClass.Image.COLUMN_NAME_TITLE)));
            image.setImageUrl(cursor.getString(cursor.getColumnIndex(ContractClass.Image.COLUMN_NAME_URL)));
            image.setInFavorites(true);
            images.add(image);
            totalCount++;
        }
        fragmentView.showResult(images);
        cursor.close();
    }

    @Override
    public void onLoadComplete(Loader<List<Image>> loader, List<Image> data) {
    }
}
