package com.example.kozak.dptesttask;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kozak.dptesttask.model.Image;
import com.example.kozak.dptesttask.util.ImageGetter;
import com.example.kozak.dptesttask.view.fragment.favfragment.FavFragmentPresenterImpl;
import com.example.kozak.dptesttask.view.fragment.webfragment.WebFragmentPresenterImpl;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private static final String TAG = "DataAdapter";
    private List<Image> images;

    public DataAdapter(List<Image> images) {
        this.images = images;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataAdapter.ViewHolder holder, int position) {
        final Image image = images.get(position);
        ImageGetter.getImage(image.getImageUrl(), holder.imageView);
        holder.checkBox.setChecked(image.isInFavorites());
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   if (holder.checkBox.isChecked()) {
                                                       FavFragmentPresenterImpl.addToFav(image);
                                                   } else {
                                                       FavFragmentPresenterImpl.removeFromFav(image);
                                                   }
                                               }
                                           }
        );
        holder.textView.setText(image.getTitle());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebFragmentPresenterImpl.showFullscreen(image.getImageUrl());
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private CheckBox checkBox;
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            checkBox = (CheckBox) itemView.findViewById(R.id.fav_checkbox);
            textView = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}

