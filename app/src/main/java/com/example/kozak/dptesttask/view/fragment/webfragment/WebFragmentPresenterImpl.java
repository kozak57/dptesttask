package com.example.kozak.dptesttask.view.fragment.webfragment;

import android.content.Context;

import com.example.kozak.dptesttask.loaders.AsyncImageLinksGetter;
import com.example.kozak.dptesttask.model.Image;

import java.util.List;


public class WebFragmentPresenterImpl implements WebFragmentPresenter {
    private static WebFragmentView fragmentView;
    private static List<Image> allImages;

    private Context context;


    public WebFragmentPresenterImpl(WebFragmentView fragment, Context context) {
        fragmentView = fragment;
        this.context = context;
    }


    @Override
    public void searchImages(String searchQuery) {
        new AsyncImageLinksGetter(context, searchQuery).loadInBackground();

    }

    @Override
    public void loadMoreImages(int totalItemsCount, String searchQuery) {
        new AsyncImageLinksGetter(context, searchQuery, String.valueOf(totalItemsCount + 1)).loadInBackground();

    }


    public static void showFullscreen(String imageUrl) {
        fragmentView.showFullScreenImage(imageUrl);
    }

    public static void updateView(List<Image> images) {
        if (allImages == null) {
            allImages = images;
            fragmentView.showResult(allImages);
        } else {
            allImages.addAll(images);

            fragmentView.updateResult(allImages);
        }
    }
}
