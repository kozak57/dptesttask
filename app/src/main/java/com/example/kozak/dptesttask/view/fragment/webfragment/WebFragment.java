package com.example.kozak.dptesttask.view.fragment.webfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.kozak.dptesttask.DataAdapter;
import com.example.kozak.dptesttask.R;
import com.example.kozak.dptesttask.ScrollListener;
import com.example.kozak.dptesttask.model.Image;
import com.example.kozak.dptesttask.view.fragment.FullScreenImageFragment;

import java.util.List;

public class WebFragment extends Fragment implements WebFragmentView {

    private static final String TAG = "WebFragment";
    private Button button;
    private EditText searchField;
    private DataAdapter dataAdapter;
    private RecyclerView recyclerView;
    private View view;
    private WebFragmentPresenterImpl presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.web_fragment, container, false);
        button = (Button) view.findViewById(R.id.bt_search);
        searchField = (EditText) view.findViewById(R.id.ed_search_field);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        presenter = new WebFragmentPresenterImpl(this, getContext());

        hideSoftKeyboard();

        recyclerView.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new ScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int totalItemsCount) {
                presenter.loadMoreImages(totalItemsCount, searchField.getText().toString());
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.searchImages(searchField.getText().toString());
                hideSoftKeyboard();
            }
        });

        return view;
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showResult(List<Image> images) {
        Log.d(TAG, Thread.currentThread().getName());
        dataAdapter = new DataAdapter(images);
        recyclerView.setAdapter(dataAdapter);
    }

    @Override
    public void updateResult(List<Image> images) {
        dataAdapter.notifyItemChanged(images.size() - 10);
    }

    @Override
    public void showFullScreenImage(String imageUrl) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Fragment fullScreenImageFragment = new FullScreenImageFragment();
        Bundle args = new Bundle();
        args.putString("ImageUrl", imageUrl);
        fullScreenImageFragment.setArguments(args);

        fragmentTransaction.add(fullScreenImageFragment, "fullscreen");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
