package com.example.kozak.dptesttask.view.fragment.webfragment;

public interface WebFragmentPresenter {

    void searchImages(String searchQuery);

    void loadMoreImages(int totalItemsCount, String searchQuery);

}
