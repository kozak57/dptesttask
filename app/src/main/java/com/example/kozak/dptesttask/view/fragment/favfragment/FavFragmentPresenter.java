package com.example.kozak.dptesttask.view.fragment.favfragment;

public interface FavFragmentPresenter {

    void showImages(int count);
}
