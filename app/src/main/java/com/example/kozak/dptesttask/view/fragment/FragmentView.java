package com.example.kozak.dptesttask.view.fragment;

import com.example.kozak.dptesttask.model.Image;

import java.util.List;

public interface FragmentView {

    void showResult(List<Image> images);

    void showFullScreenImage(String imageUrl);
}
