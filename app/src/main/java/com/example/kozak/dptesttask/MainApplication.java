package com.example.kozak.dptesttask;

import android.app.Application;
import android.content.Context;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

public class MainApplication extends Application {
    private static Context context;

    @Override

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(false);
        Picasso.setSingletonInstance(built);
    }

    public static Context getContext() {
        return context;
    }
}
