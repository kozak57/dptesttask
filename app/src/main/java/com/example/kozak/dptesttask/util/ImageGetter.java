package com.example.kozak.dptesttask.util;

import android.widget.ImageView;

import com.example.kozak.dptesttask.MainApplication;
import com.squareup.picasso.Picasso;

public class ImageGetter {
    public static void getImage(String url, ImageView imageView) {
        Picasso.with(MainApplication.getContext()).load(url).into(imageView);
    }
}
