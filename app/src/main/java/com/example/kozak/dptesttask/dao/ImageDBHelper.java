package com.example.kozak.dptesttask.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kozak.dptesttask.model.ContractClass;


public class ImageDBHelper extends SQLiteOpenHelper {

    private static final String TAG = "ImageDBHelper";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Images.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_IMAGES_TABLE =
            "CREATE TABLE " + ContractClass.Image.TABLE_NAME + " (" +
                    ContractClass.Image.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    ContractClass.Image.COLUMN_NAME_URL + TEXT_TYPE +
                    " )";

    public ImageDBHelper(Context context, String name) {
        super(context, name, null, DATABASE_VERSION);
    }

    private void fillInitialData(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(ContractClass.Image.COLUMN_NAME_TITLE, "Sample Image");
        values.put(ContractClass.Image.COLUMN_NAME_URL, "https://i.ytimg.com/vi/tntOCGkgt98/maxresdefault.jpg");

        db.insert(ContractClass.Image.TABLE_NAME, null, values);

        values.clear();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_IMAGES_TABLE);
        fillInitialData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ContractClass.Image.TABLE_NAME);
        onCreate(db);
    }
}
