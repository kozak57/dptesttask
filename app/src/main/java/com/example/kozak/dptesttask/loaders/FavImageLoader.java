package com.example.kozak.dptesttask.loaders;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.example.kozak.dptesttask.dao.ImageDao;

public class FavImageLoader extends CursorLoader {
    private ImageDao imageDao;

    public FavImageLoader(Context context) {
        super(context);
        imageDao = ImageDao.getInstance(context);
    }

    @Override
    public Cursor loadInBackground() {


        return imageDao.getImagesInCursor();
    }
}
