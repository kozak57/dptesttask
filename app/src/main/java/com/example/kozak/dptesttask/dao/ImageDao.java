package com.example.kozak.dptesttask.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.kozak.dptesttask.model.ContractClass;
import com.example.kozak.dptesttask.model.Image;

public class ImageDao {
    public static final String TAG = "TrackDao";

    private ImageDBHelper db;
    private static ImageDao instance;

    private ImageDao(Context context) {
        db = new ImageDBHelper(context, ImageDBHelper.DATABASE_NAME);
    }

    public ImageDao(ImageDBHelper imageDBHelper) {
        db = imageDBHelper;
    }

    public static ImageDao getInstance(Context context) {

        if (instance == null) {
            synchronized (ImageDao.class) {
                if (instance == null) {
                    instance = new ImageDao(context);
                }
            }
        }
        return instance;
    }

    public void addImage(Image image) {
        ContentValues values = new ContentValues();
        values.put(ContractClass.Image.COLUMN_NAME_TITLE, image.getTitle());
        values.put(ContractClass.Image.COLUMN_NAME_URL, image.getImageUrl());

        db.getWritableDatabase().insert(ContractClass.Image.TABLE_NAME, null, values);
    }

    public Cursor getImagesInCursor() {

        return db.getReadableDatabase().rawQuery("SELECT * FROM " + ContractClass.Image.TABLE_NAME + ";", null);
    }

    public void removeImage(Image image) {
        db.getWritableDatabase().delete(ContractClass.Image.TABLE_NAME, ContractClass.Image.COLUMN_NAME_URL + "= ?", new String[]{image.getImageUrl()});
    }
}
